import java.util.ArrayList;

public class Automovil {
	
	private Motor motor;
	private Estanque estanque;
	private Velocimetro velocimetro;
	private ArrayList<Rueda> rueda;
	private boolean encendido;
	private double consumo;
	private int cambiar_rueda = 0;
	private int sin_gasolina = 0;
	private double distancia;
	private double distancia_total = 0;
	
	Automovil( Motor motor, Estanque estanque, Velocimetro velocimetro){
		
		this.rueda = new ArrayList<Rueda>();
		this.encendido = false;	
	}
	
	public Motor getMotor() {
		
		return motor;
	}
	
	public void setMotor(Motor motor) {
		
		this.motor = motor;
	}
	
   public int getCambiar_rueda() {
		
		return cambiar_rueda;
	}
	
	public void setCambiar_rueda(int cambiar_rueda) {
		
		this.cambiar_rueda = cambiar_rueda;
	}
	
    public int  getSin_gasolina() {
		
		return sin_gasolina;
	}
	
	public void setSin_gasolina(int sin_gasolina) {
		
		this.sin_gasolina = sin_gasolina;
	}
	
	public Estanque getEstanque(){
		
		return estanque;
	}
	
	public void setEstanque(Estanque estanque) {
		
		this.estanque = estanque;
	}
	
	public Velocimetro getVelocimetro() {
		
		return velocimetro;
		
	}
	public void setVelocimetro(Velocimetro velocimetro) {
		this.velocimetro = velocimetro;
	}
	
	public ArrayList<Rueda> getRueda(){
		
		return rueda;
	}
	
	public void setRueda(Rueda rueda) {
		
		this.rueda.add(rueda);
	}
	public double getConsumo() {
		
		return consumo;
	}
	
	public void encender_automovil(Estanque estanque) {
		consumo = 0;
		this.encendido = true;
		consumo = estanque.getCantidad_de_combustible()/100;
		consumo = estanque.getCantidad_de_combustible() - consumo;
		estanque.setCantidad_de_combustible(consumo);
		System.out.println("Cantidad de combustible despues de encender:" + estanque.getCantidad_de_combustible()+"l");
		
	}
	
	
	public void desgastamiento_rueda() {
		int n = 0;
		for (Rueda r : this.rueda) {
			if (distancia_total == 0) {
				
				int i = 0;
				n = n +1 ;
				r.setDesgaste(i); 
				System.out.println("Rueda N�" + n +" desgaste : "+r.getDesgaste()+"%");
				
			}else {
				int i = ( int)(Math.random()*10+1);
				n = n +1 ;
				r.setDesgaste(i); 
				System.out.println("Rueda N�" + n +" desgaste : "+r.getDesgaste()+"%");
			}
            if (r.getDesgaste() >= 90) {
        
				this.encendido = false;
				cambiar_rueda = 1;
                int o = 0;
                for (Rueda m : this.rueda) {  
           	
                   m.setDesgaste(o);
	
                }
             } 
		}
	}
	 public double Distancia(Velocimetro velocimetro) {
    	 if (this.encendido == false) {
    		 
    		 velocimetro.setVelocidad(0);
    		 velocimetro.setTiempo(0);
    		 double velocidad = velocimetro.getVelocidad();
    		 int tiempo = velocimetro.getTiempo();
    		 distancia = tiempo * velocidad;
    		 distancia_total = distancia + distancia_total;

    		 
    	 }else {
    		 velocimetro.setVelocidad((int)(Math.random()*120+1));
    		 velocimetro.setTiempo(( int)(Math.random()*10+1));
    		 double velocidad = velocimetro.getVelocidad();
    		 int tiempo = velocimetro.getTiempo();
    		 distancia = tiempo * velocidad;
    		 distancia_total = distancia + distancia_total;
	    
		}
    	 return distancia; 
    	 
	}
	public void consumo_gasolina(Motor motor, Velocimetro velocimetro, Estanque estanque) {
		
		consumo = 0;
		
		if (motor.getCilindrada() == 0) {//es igual a decir, que es un cilindrada de 1,2
			
			consumo = (distancia/20);
            if (consumo >= estanque.getCantidad_de_combustible()) {
				
				consumo = estanque.getCantidad_de_combustible();
				distancia = consumo*20;
				int tiempo = velocimetro.getTiempo();
				velocimetro.setTiempo(tiempo);
				double velocidad = distancia/tiempo;
				velocimetro.setVelocidad(velocidad); 
			
			}
			
			 consumo =  estanque.getCantidad_de_combustible()-consumo;
			 estanque.setCantidad_de_combustible(consumo);
		/*	  }*/
	  }else if (motor.getCilindrada() == 1) {//es el caso de la cilindrada de 1,6
			
			consumo = distancia/14;
			if (consumo >= estanque.getCantidad_de_combustible()) {
				
				consumo = estanque.getCantidad_de_combustible();
				distancia = consumo*14;
				int tiempo = velocimetro.getTiempo();
				velocimetro.setTiempo(tiempo);
				double velocidad = distancia/tiempo;
				velocimetro.setVelocidad(velocidad); 
			}
			consumo =  estanque.getCantidad_de_combustible()-consumo;
			estanque.setCantidad_de_combustible(consumo);
			
		}
	  if(consumo <= 0) {
			sin_gasolina = 1;
		}
	
	
	}
    public void imprimir(Velocimetro velocimetro, Estanque estanque , Motor motor) {
    	
    	System.out.println("Cantidad de combustible: " + estanque.getCantidad_de_combustible()+" l");
    	System.out.println("Distancia total: " + distancia_total +" Kilometros");
    	System.out.println("Velocidad: " + velocimetro.getVelocidad() + " km/s");
		System.out.println("Tiempo: " + velocimetro.getTiempo() + " s");
		System.out.println("Distancia: " + distancia + " km");
		if(motor.getCilindrada() == 0) {
			System.out.println("Su cilindrada es de: 1,2");
		}else {
		  System.out.println("Su cilindrada es de: 1,6");
		}
    }
    
}