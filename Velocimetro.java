
public class Velocimetro {
	
	private double velocidad;
	private int tiempo;
	
    Velocimetro(){
    	velocidad = (double)(Math.random()*120+1);
    	tiempo = ( int)(Math.random()*10+1);
    } 
    
	public double getVelocidad() {
		
		return velocidad;
	}
	
	public void setVelocidad(double velocidad) {
		
		this.velocidad = velocidad;
	}
    
    public int getTiempo() {
    	
		return tiempo;
	}

	public void setTiempo(int tiempo) {
		
		this.tiempo = tiempo;
	}
     
	
}