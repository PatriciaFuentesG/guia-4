
public class Rueda {
	
	private int desgaste = 0;
	
	Rueda(){}
	
	public int getDesgaste() {
		
		return desgaste;
	}
	
	public void setDesgaste(int i) {
		
		this.desgaste = desgaste + i;
		if (i == 0) {
			this.desgaste = 0;
		}
	}

}
